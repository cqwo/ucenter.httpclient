/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆英卡电子有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.ucenter.client.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

//用户
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PartUserInfo implements Serializable {


    private static final long serialVersionUID = -812194532736924613L;

    /**
     * 用户Uid
     **/
    private String uid = "";

    /**
     * 用户名
     **/
    private String userName = "";


    /**
     * 邮箱
     **/
    private String email = "";

    /**
     * 手机
     **/
    private String mobile = "";

    /**
     * 密码
     **/
    private String password = "";

    /**
     * 用户等级
     **/
    private Integer userRid = 0;

    /**
     * 昵称
     **/
    private String nickName = "";

    /**
     * 推荐人
     */
    private Integer parentId = 0;

    /**
     * 真实姓名
     **/
    private String realName = "";

    /**
     * 头像
     **/
    private String avatar = "";

    /**
     * 所在区域
     **/
    private Integer regionId = 0;

    /**
     * 地址
     **/
    private String address = "";

    /**
     * 经度
     **/
    private double longitude = 0.00;

    /**
     * 纬度
     **/
    private double latitude = 0.00;
    /**
     * 可支付积分
     **/
    private Integer payCredits = 0;

    /**
     * 积分等级
     **/
    private Integer rankCredits = 0;

    /**
     * 用户余额
     **/
    private double money = 0.00;

    /**
     * 消费等级
     **/
    private double rankMoney = 0.00;

    /**
     * 邀请码
     */
    private String invitCode = "";

    /**
     * 是否验证邮箱
     **/
    private Integer verifyEmail = 0;
    /**
     * 是否验证手机
     **/
    private Integer verifyMobile = 0;
    /**
     * 解禁时间
     **/
    private Integer liftBanTime = 0;
    /**
     * 盐值
     **/
    private String salt = "";


//    public PartUserInfo(String uid, String userName, String email, String mobile, Integer userRid, String nickName, String realName) {
//        this.uid = uid;
//        this.userName = userName;
//        this.email = email;
//        this.mobile = mobile;
//        this.userRid = userRid;
//        this.nickName = nickName;
//        this.realName = realName;
//    }
//
//    public PartUserInfo(String userName, String password) {
//        this.userName = userName;
//        this.password = password;
//    }


}