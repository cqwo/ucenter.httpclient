/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆英卡电子有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.ucenter.client.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.compile;
import static java.util.regex.Pattern.matches;

/**
 * 开放授权
 *
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OauthInfo implements Serializable {

    private static final long serialVersionUID = -6406139675956385119L;

    /**
     * 开放授权Id
     **/

    private Integer id = 0;


    /**
     * Uid
     **/
    private String uid = "";

    /**
     * appid
     **/
    private String appId = "";

    /**
     * openid
     **/
    private String openId = "";

    /**
     * unionid
     **/
    private String unionId = "";

    /**
     * 服务商
     **/
    private String server = "wechat";


//    public OauthInfo(String uid, String openId, String unionId, String server) {
//        this.uid = uid;
//        this.openId = openId;
//        this.unionId = unionId;
//        this.server = server;
//    }

    public static void main(String[] args) {
//
//        String str = "FEFEFE21211121323223FEFEFEC323222FAFBFC";
//
//        String regEx = "(FEFEFE)[A-Za-z0-9]+(FAFBFC)";
//        // 编译正则表达式
//        Pattern pattern = Pattern.compile(regEx, Pattern.CASE_INSENSITIVE);
//        // 忽略大小写的写法
//        // Pattern pat = Pattern.compile(regEx, Pattern.CASE_INSENSITIVE);
//        Matcher matcher = pattern.matcher(str);
//
//        int count = 0;
//
//        while (matcher.find()) {
//            count++;
//            System.out.println("Match number " + count);
//            System.out.println("start(): " + matcher.start());
//            System.out.println("end(): " + matcher.end());
//            System.out.println(matcher.toString());
//        }


        //String[] str2 = p.split("FEFEFE21211121323223FEFEFEC323222FAFBFC");

        String str = "323222FEFEFE212111FEFE21323223FEFEFEC323222FAFBFC";

        String p = "^(FEFEFE)[A-Za-z0-9]+?(FAFBFC)$";

        Pattern p2 = compile("(FEFEFE)|(FEFE)");

        String[] ss = p2.split(str);

        for (String s : ss) {

            System.out.println("2121:" + s);

            if (matches(p, s)) {
                System.out.println(s);
            }


        }


    }


}