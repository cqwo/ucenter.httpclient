/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆英卡电子有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.ucenter.client.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 用户等级
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRankInfo implements Serializable {

    private static final long serialVersionUID = -5299034745544946079L;

    /**
     * 用户等级id
     **/

    private Integer userRid = 6;


    /**
     * 是否是系统等级
     **/
    private Integer system = 1;


    /**
     * 用户等级标题
     **/
    private String title = "6";

    /**
     * 用户等级头像
     **/
    private String avatar = "";

    /**
     * 用户等级积分上限
     **/
    private Integer creditsLower = 0;

    /**
     * 用户等级积分下限
     **/
    private Integer creditsUpper = 0;

    /**
     * 限制天数
     **/
    private Integer limitDays = 0;


}