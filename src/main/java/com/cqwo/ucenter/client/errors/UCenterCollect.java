package com.cqwo.ucenter.client.errors;

/**
 * @Author: cqnews
 * @Date: 2018-09-28 15:06
 * @Version 1.0
 */
public class UCenterCollect {

    /**
     * 解析失败
     */
    public static final Integer RESOLVE_FAILED = -3;

    /**
     * 降级方案
     */
    public static final Integer DOWNGRADE = -2;

    /**
     * 网络异常
     */
    public static final Integer NETWORK_FAILED = -1;

    /**
     * 成功
     */
    public static final Integer SUCCESS = 0;

    /**
     * 错误
     */
    public static final Integer ERROR = 2;

    /**
     * 无权限
     */
    public static final Integer AUTHOR_FAILED = 1000;


    /**
     * 用户未登录
     */
    public static final Integer AUTHOR_NOLOGIN = 1100;


    /**
     * 错误登录
     */
    public static final Integer AUTHOR_ERRLOGIN = 1200;


    /**
     * 错误的手机号码
     */
    public static final Integer AUTHOR_ERRMOBILE = 1210;


    /**
     * 错误的手机号码
     */
    public static final Integer AUTHOR_ERRWECHAT = 1220;


    /**
     * 未找到用户信息
     */
    public static final Integer AUTHOR_UNKNOWNACCOUNT = 1230;

    /**
     * 账号密码错误
     */
    public static final Integer AUTHOR_INCORRECTCREDENTIALS = 1240;

    /**
     * 账号密码错误
     */
    public static final Integer AUTHOR_LOCKEDACCOUNT = 1250;
    /**
     * 账号密码错误
     */
    public static final Integer AUTHOR_EXCESSIVEATTEMPTS = 1260;
    /**
     * 账号密码错误
     */
    public static final Integer AUTHOR_AUTHENTICATION = 1270;

    /**
     * 错误的登录信息
     */
    public static final Integer AUTHOR_ERRREGISTER = 1300;


    /**
     * 错误的登录信息
     */
    public static final Integer AUTHOR_REPEATUSER = 1310;

    /**
     * 错误的登录信息
     */
    public static final Integer AUTHOR_FAILUSER = 1320;

    /**
     * token过期
     */
    public static final Integer TOKEN_EXPIRE = 1400;

    /**
     * token错误
     */
    public static final Integer TOKEN_ERROR = 1410;


    /**
     * 校验证失败
     */
    public static final Integer VALIDATION_FAILED = 2000;


}
