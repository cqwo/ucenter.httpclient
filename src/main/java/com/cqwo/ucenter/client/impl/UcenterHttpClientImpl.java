package com.cqwo.ucenter.client.impl;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cqwo.ucenter.client.UCenterHttpClient;
import com.cqwo.ucenter.client.domain.LoginSuccesInfo;
import com.cqwo.ucenter.client.domain.OauthInfo;
import com.cqwo.ucenter.client.domain.PartUserInfo;
import com.cqwo.ucenter.client.domain.RefreshTokenInfo;
import com.cqwo.ucenter.client.errors.UCenterCollect;
import com.cqwo.ucenter.client.exception.*;
import com.cqwo.ucenter.client.helper.HttpHelper;
import com.cqwo.ucenter.client.helper.JsonHelper;
import com.cqwo.ucenter.client.message.UCenterMessageInfo;
import com.cqwo.ucenter.client.pages.PageInfo;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.Map;


/**
 * @author cqnews
 */
public abstract class UcenterHttpClientImpl implements UCenterHttpClient {


    public final String HEADER_API_APIKEY = "X-CWMAPI-ApiKey";

    public final String HEADER_API_APISECRET = "X-CWMAPI-ApiSecret";

    public final String HEADER_API_TOKEN = "X-CWMAPI-Token";

    public final String HEADER_API_OPENID = "X-CWMAPI-OpenId";

    public final String HEADER_API_APPID = "X-CWMAPI-AppId";


    /**
     * 日志
     */
    private Logger logger = LoggerFactory.getLogger(UcenterHttpClientImpl.class);


    /**
     * Post提交数据
     *
     * @param url  url地址
     * @param maps 参数
     * @param clzz 类型
     * @param <T>  类型
     * @return 返回
     * @throws UCenterException
     */
    @Override
    public <T> T restPost(String url, Map<String, Object> maps, Class<T> clzz) throws UCenterException {
        return restPost(url, maps, "", clzz);
    }

    @Override
    public <T> T restPost(String url, Map<String, Object> maps, String token, Class<T> clzz) throws UCenterFailException {

        try {

            Map<String, String> headers = Maps.newHashMap();
            headers.put(HEADER_API_TOKEN, token);
            headers.put(HEADER_API_APPID, getAppId());
            headers.put(HEADER_API_APIKEY, getApiKey());
            headers.put(HEADER_API_APISECRET, getApiSecret());

            String result = HttpHelper.getInstance().post(url, maps, headers);

            return JSON.parseObject(result, clzz);

        } catch (Exception ex) {
            ex.printStackTrace();
            throw new UCenterFailException(UCenterCollect.NETWORK_FAILED, "网络异常");
        }
    }

    @Override
    public <T> T restGet(String url, String token, Class<T> clzz) throws UCenterException {

        try {

            Map<String, String> headers = Maps.newHashMap();
            headers.put(HEADER_API_TOKEN, token);
            headers.put(HEADER_API_APIKEY, getApiKey());
            headers.put(HEADER_API_APISECRET, getApiSecret());

            String result = HttpHelper.getInstance().get(url, headers);

            return JSON.parseObject(result, clzz);
        } catch (Exception ex) {
            logger.warn(ex.getMessage());
            throw new UCenterFailException(-1, "网络异常");
        }


    }

    //region 用户

    /**
     * 用户注册
     *
     * @param account  注册账号
     * @param password 密码
     * @param nickName 昵称
     * @param avatar   头像
     * @param gender   性别
     * @param regionId 区域
     * @throws UCenterException 用户自定义异常
     */
    @Override
    public void onRegister(String account,
                           String password,
                           String nickName,
                           String avatar,
                           Integer gender,
                           Integer regionId) throws UCenterException {
        onRegister(account, password, nickName, avatar, gender, regionId, 2);
    }


    /**
     * 用户注册
     *
     * @param account  注册账号
     * @param password 密码
     * @param nickName 昵称
     * @param avatar   头像
     * @param gender   性别
     * @param regionId 区域
     */
    @Override
    public void onRegister(String account,
                           String password,
                           String nickName,
                           String avatar,
                           Integer gender,
                           Integer regionId,
                           Integer expireHours) throws UCenterException {

        Map<String, Object> maps = Maps.newHashMap();
        maps.put("account", account);
        maps.put("password", password);
        maps.put("nickName", nickName);
        maps.put("avatar", avatar);
        maps.put("gender", gender);
        maps.put("regionId", regionId);
        maps.put("expireHours", expireHours);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("account/register"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {

            LoginSuccesInfo loginInfo = JSONObject.parseObject(messageInfo.getContent(), LoginSuccesInfo.class);

            if (loginInfo == null || Strings.isNullOrEmpty(loginInfo.getToken())) {

                throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
            }

            throw new UCenterLoginSuccessException(loginInfo.getUid(), loginInfo.getToken(), loginInfo.getRefreshToken(), loginInfo.getUserInfo(), loginInfo.getUserRankInfo(), messageInfo.getMessage());

        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());

    }

    /**
     * 用户登录
     *
     * @param account  账号
     * @param password 密码
     * @throws UCenterException 用户自定义异常
     */
    @Override
    public void onLogin(String account, String password) throws UCenterException {
        onLogin(account, password, true, 2);
    }

    /**
     * 用户登录
     *
     * @param account     账号
     * @param password    密码
     * @param expireHours 超时时间
     * @throws UCenterException 异常
     */
    @Override
    public void onLogin(String account,
                        String password,
                        boolean isRefresh,
                        Integer expireHours) throws UCenterException {


        Map<String, Object> maps = Maps.newHashMap();
        maps.put("account", account);
        maps.put("password", password);
        maps.put("isRefresh", isRefresh ? 1 : 0);
        maps.put("expireHours", expireHours);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("account/login"), maps, UCenterMessageInfo.class);
        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }
        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {

            LoginSuccesInfo loginInfo = JSONObject.parseObject(messageInfo.getContent(), LoginSuccesInfo.class);

            if (loginInfo == null || Strings.isNullOrEmpty(loginInfo.getToken())) {

                throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
            }

            throw new UCenterLoginSuccessException(loginInfo.getUid(), loginInfo.getToken(), loginInfo.getRefreshToken(), loginInfo.getUserInfo(), loginInfo.getUserRankInfo(), messageInfo.getMessage());


        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
    }


    /**
     * 用户token登录
     *
     * @param token token
     * @throws UCenterException 返回用户登录信息
     */
    @Override
    public void onLogin(String token) throws UCenterException {

        Map<String, Object> maps = Maps.newHashMap();
        maps.put("token", token);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("account/tokenlogin"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {

            LoginSuccesInfo loginInfo = JSONObject.parseObject(messageInfo.getContent(), LoginSuccesInfo.class);

            if (loginInfo == null || Strings.isNullOrEmpty(loginInfo.getToken())) {

                throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
            }

            throw new UCenterLoginSuccessException(loginInfo.getUid(), loginInfo.getToken(), loginInfo.getRefreshToken(), loginInfo.getUserInfo(), loginInfo.getUserRankInfo(), messageInfo.getMessage());


        } else if (UCenterCollect.TOKEN_EXPIRE.equals(messageInfo.getState())) {

            throw new UcenterTokenExpireExeption(messageInfo.getState(), messageInfo.getMessage());

        } else if (UCenterCollect.TOKEN_ERROR.equals(messageInfo.getState())) {

            throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());

    }


    /**
     * 开发接口登录
     *
     * @param openId   opendId
     * @param unionId  联合id
     * @param nickName 昵称
     * @param avatar   头像
     * @param gender   性别
     * @param regionId 区域
     */
    @Override
    public UCenterMessageInfo onLogin(String server,
                                      String openId,
                                      String unionId,
                                      String nickName,
                                      String avatar,
                                      Integer gender,
                                      Integer regionId) throws UCenterException {
        return onLogin(server, openId, unionId, nickName, avatar, gender, regionId, 2);
    }

    /**
     * 开发接口登录
     *
     * @param openId      opendId
     * @param unionId     联合id
     * @param nickName    昵称
     * @param avatar      头像
     * @param gender      性别
     * @param regionId    区域
     * @param expireHours 超时时间
     */
    @Override
    public UCenterMessageInfo onLogin(String server,
                                      String openId,
                                      String unionId,
                                      String nickName,
                                      String avatar,
                                      Integer gender,
                                      Integer regionId,
                                      Integer expireHours) throws UCenterException {


        Map<String, Object> maps = Maps.newHashMap();
        maps.put("server", server);
        maps.put("openId", openId);
        maps.put("unionId", unionId);
        maps.put("nickName", nickName);
        maps.put("avatar", avatar);
        maps.put("gender", gender);
        maps.put("regionId", regionId);
        maps.put("expireHours", expireHours);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("oauth/login"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {

            LoginSuccesInfo loginInfo = JSONObject.parseObject(messageInfo.getContent(), LoginSuccesInfo.class);

            if (loginInfo == null || Strings.isNullOrEmpty(loginInfo.getToken())) {

                throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
            }

            throw new UCenterLoginSuccessException(loginInfo.getUid(), loginInfo.getToken(), loginInfo.getRefreshToken(), loginInfo.getUserInfo(), loginInfo.getUserRankInfo(), messageInfo.getMessage());


        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());


    }


    /**
     * 更新一条用户信息数据
     *
     * @param uid      更新用户uid
     * @param nickName 更新昵称
     * @param realName 更新真实姓名
     * @param regionId 更新区域Id
     * @param address  更新地址
     * @param bio      更新描述
     * @return 更新信息
     * @throws UCenterException Exception
     */
    @Override
    public PartUserInfo updateUser(String uid,
                                   String nickName,
                                   String realName,
                                   int regionId,
                                   String address,
                                   String bio) throws UCenterException {

        Map<String, Object> maps = Maps.newHashMap();
        maps.put("uid", uid);
        maps.put("nickName", nickName);
        maps.put("realName", realName);
        maps.put("regionId", regionId);
        maps.put("address", address);
        maps.put("bio", bio);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("user/update"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {


            return JSON.parseObject(messageInfo.getContent(), PartUserInfo.class);

        }
        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
    }


    /**
     * 修改用户密码
     *
     * @param mobile      手机号
     * @param newPassword 新密码
     * @return
     * @throws UCenterException
     */
    @Override
    public PartUserInfo updatePassword(String mobile,
                                       String newPassword) throws UCenterException {

        Map<String, Object> maps = Maps.newHashMap();
        maps.put("mobile", mobile);
        maps.put("newPassword", newPassword);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("user/updatepassword"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {
            return JSON.parseObject(messageInfo.getContent(), PartUserInfo.class);
        }
        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
    }

    /**
     * 修改用户密码
     *
     * @param uid         Uid
     * @param newPassword 新密码
     * @return
     * @throws UCenterException
     */
    @Override
    public PartUserInfo resetPassword(String uid,
                                      String newPassword) throws UCenterException {

        Map<String, Object> maps = Maps.newHashMap();
        maps.put("uid", uid);
        maps.put("newPassword", newPassword);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("user/resetpassword"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {
            return JSON.parseObject(messageInfo.getContent(), PartUserInfo.class);
        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
    }

    /**
     * 刷新token
     *
     * @param uid          uid
     * @param refreshToken 刷新token
     * @return RefreshTokenInfo
     */
    @Override
    public RefreshTokenInfo refreshToken(String uid, String refreshToken) throws UCenterException {
        return refreshToken(uid, refreshToken, 2);
    }

    /**
     * 刷新token
     *
     * @param uid          uid
     * @param refreshToken 刷新token
     * @return RefreshTokenInfo
     */
    @Override
    public RefreshTokenInfo refreshToken(String uid, String refreshToken, Integer expireHours) throws UCenterException {


        Map<String, Object> maps = Maps.newHashMap();
        maps.put("uid", uid);
        maps.put("refreshToken", refreshToken);
        maps.put("expireHours", expireHours);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("token/refresh"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {
            return JSON.parseObject(messageInfo.getContent(), RefreshTokenInfo.class);
        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
    }


    /**
     * 通过用户uid获取用户信息
     *
     * @param uid uid
     * @return 用户信息
     * @throws UCenterException 异常
     */
    @Override
    public PartUserInfo getPartUserByUid(String uid) throws UCenterException {

        Map<String, Object> maps = Maps.newHashMap();
        maps.put("uid", uid);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("user/findbyuid"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {
            return JSON.parseObject(messageInfo.getContent(), PartUserInfo.class);
        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
    }


    /**
     * 通过手机号码获取用户
     *
     * @return 用户信息
     */
    @Override
    public PartUserInfo getPartUserByMobile(String mobile) throws UCenterException {

        Map<String, Object> maps = Maps.newHashMap();
        maps.put("mobile", mobile);


        UCenterMessageInfo messageInfo = restPost(getApiUrl("user/findbymobile"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {
            return JSON.parseObject(messageInfo.getContent(), PartUserInfo.class);
        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
    }

    /**
     * 通过用户名获取用户
     *
     * @param userName 用户名
     * @return 用户信息
     */
    @Override
    public PartUserInfo getPartUserByUserName(String userName) throws UCenterException {


        Map<String, Object> maps = Maps.newHashMap();
        maps.put("userName", userName);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("user/findbyusername"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {
            return JSON.parseObject(messageInfo.getContent(), PartUserInfo.class);
        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
    }


    /**
     * 通过邮箱获取用户信息
     *
     * @param email 邮箱
     */
    @Override
    public PartUserInfo getPartUserByEmail(String email) throws UCenterException {

        Map<String, Object> maps = Maps.newHashMap();
        maps.put("email", email);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("user/findbyusername"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {
            return JSON.parseObject(messageInfo.getContent(), PartUserInfo.class);
        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
    }

    /**
     * 通过token获取用户信息
     *
     * @param openId openid
     */
    @Override
    public PartUserInfo getPartUserByOpenId(String openId) throws UCenterException {

        Map<String, Object> maps = Maps.newHashMap();
        maps.put("openId", openId);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("user/findbyopenid"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {
            return JSON.parseObject(messageInfo.getContent(), PartUserInfo.class);
        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
    }

    /**
     * 通过unionId获取用户信息
     *
     * @param unionId unionId
     */
    @Override
    public PartUserInfo getPartUserByUnionId(String unionId) throws UCenterException {

        Map<String, Object> maps = Maps.newHashMap();
        maps.put("unionId", unionId);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("user/findbyunionid"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {
            return JSON.parseObject(messageInfo.getContent(), PartUserInfo.class);
        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());

    }


    /**
     * 获得用户数据列表
     *
     * @param uid      uid
     * @param nickName 昵称
     * @param mobile   手机号
     * @return 返回UserInfo
     **/
    @Override
    public List<PartUserInfo> getPartUserList(String uid, String nickName, String mobile) throws UCenterException {

        Map<String, Object> maps = Maps.newHashMap();
        maps.put("uid", uid);
        maps.put("nickName", nickName);
        maps.put("mobile", mobile);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("user/list"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {

            JSONObject object = JSONObject.parseObject(messageInfo.getContent());

            return JSONArray.parseArray(JsonHelper.getString(object, "userInfoList"), PartUserInfo.class);

        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());

    }


    /**
     * 获取用户列表
     *
     * @param pageSize   每页条数
     * @param pageNumber 当前页数
     * @param uid        uid
     * @param nickName   昵称
     * @param mobile     手机
     * @return 用户列表信息
     * @throws UCenterException 异常
     */
    @Override
    public void getPartUserList(Integer pageSize, Integer pageNumber, String uid, String nickName, String mobile) throws UCenterException {

        Map<String, Object> maps = Maps.newHashMap();
        maps.put("pageSize", pageSize.toString());
        maps.put("pageNumber", pageNumber.toString());
        maps.put("uid", uid);
        maps.put("nickName", nickName);
        maps.put("mobile", mobile);


        UCenterMessageInfo messageInfo = restPost(getApiUrl("user/list"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {

            JSONObject object = JSONObject.parseObject(messageInfo.getContent());

            List<PartUserInfo> userInfoList = JSONArray.parseArray(JsonHelper.getString(object, "userInfoList"), PartUserInfo.class);
            Integer totalCount = JsonHelper.getInteger(object, "totalCount");

            PageInfo<PartUserInfo> pageInfo = new PageInfo<>(pageSize, pageNumber, totalCount, userInfoList);

            throw new UCenterPageListSuccessException(pageInfo, messageInfo.getMessage());
        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
    }


    /**
     * 判断是否有重复的code
     *
     * @param invitCode 邀请码
     * @return
     * @throws IOException
     */
    public boolean isExitsInvitCode(String invitCode) throws UCenterException {
        return false;
    }

    /**
     * 更新用户组
     *
     * @param uid     用户uid
     * @param userRid 用户分组
     */
    @Override
    public void updateUserRankByUid(String uid, Integer userRid) throws UCenterException {

        Map<String, Object> maps = Maps.newHashMap();
        maps.put("uid", uid);
        maps.put("userRid", userRid);


        UCenterMessageInfo messageInfo = restPost(getApiUrl("user/list"), maps, UCenterMessageInfo.class);
    }

    /**
     * 更新手机
     *
     * @param uid    uid
     * @param mobile 手机
     */
    @Override
    public void updateUserMobile(String uid, String mobile) throws UCenterException {

        Map<String, Object> maps = Maps.newHashMap();
        maps.put("uid", uid);
        maps.put("mobile", mobile);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("user/updateusermobile"), maps, UCenterMessageInfo.class);

    }

    /**
     * 更新用户金额
     *
     * @param uid   用户uid
     * @param money 金额
     */
    @Override
    public void updateUserMoneyByUid(String uid, double money) throws UCenterException {

        Map<String, Object> maps = Maps.newHashMap();
        maps.put("uid", uid);
        maps.put("money", money);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("user/updatemoney"), maps, UCenterMessageInfo.class);
    }


    /**
     * 获取用户头像
     *
     * @param uid uid
     */
    @Override
    public String getUserAvatar(String uid) throws UCenterException {

        Map<String, Object> maps = Maps.newHashMap();
        maps.put("uid", uid);

        return restPost(getApiUrl("user/avatar"), maps, String.class);
    }

    /**
     * 通过appid和uid查询第三方登录信息
     *
     * @param uid uid
     */
    @Override
    public OauthInfo findOauthByUid(String uid) throws UCenterException {

        Map<String, Object> maps = Maps.newHashMap();
        maps.put("uid", uid);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("oauth/find"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {

            return JSON.parseObject(messageInfo.getContent(), OauthInfo.class);

        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
    }


    //endregion


}
