package com.cqwo.ucenter.client.helper;

import com.google.common.collect.Lists;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class HttpHelper {

    private static HttpHelper httpHelper = null;

    public synchronized static HttpHelper getInstance() {

        if (httpHelper == null) {
            httpHelper = new HttpHelper();
        }

        return httpHelper;
    }

    private HttpHelper() {
    }


    public String post(String url, Map<String, Object> maps, Map<String, String> headers) throws IOException {


        HttpClient client = null;

        try {

            client = new HttpClient();


            client.getHttpConnectionManager().getParams().setConnectionTimeout(2000);//设置连接超时时间为2秒（连接初始化时间）

            PostMethod method = new PostMethod(url);
            method.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "utf-8");

            method.setRequestHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36");
            method.setRequestHeader("Accept-Charset", "utf-8,GB2312;q=0.7,*;q=0.7");
            method.setRequestHeader("Accept-Language", "zh-cn,zh;q=0.5");
            method.setRequestHeader("Connection", "Keep-Alive");
            method.setRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");


            for (Map.Entry<String, String> entry : headers.entrySet()) {
                method.setRequestHeader(entry.getKey(), entry.getValue());
            }


            if (maps != null && maps.size() >= 1) {

                List<NameValuePair> list = Lists.newArrayList();

                // 遍历map，设置参数到list中
                for (Map.Entry<String, Object> entry : maps.entrySet()) {
                    list.add(new NameValuePair(entry.getKey(), entry.getValue().toString()));
                }

                NameValuePair[] params = new NameValuePair[list.size()];

                method.setRequestBody(list.toArray(params));

            }

            int statusCode = client.executeMethod(method);

            if (statusCode == 200) {
                System.out.println("Notification sent successfully.");
                return method.getResponseBodyAsString();
            } else {
                System.out.println("Failed to send the notification!");
            }

            throw new IOException(method.getResponseBodyAsString());

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (client != null) {
                client.getHttpConnectionManager().closeIdleConnections(1);
            }
        }

        throw new IOException("网络异常");
    }


    /**
     * get 方法
     *
     * @param url     url
     * @param headers 头部文件
     * @return 返回参数
     * @throws IOException IO异常
     */
    public String get(String url, Map<String, String> headers) throws IOException {


        HttpClient client = null;

        try {

            client = new HttpClient();
            client.getHttpConnectionManager().getParams().setConnectionTimeout(2000);//设置连接超时时间为2秒（连接初始化时间）

            GetMethod method = new GetMethod(url);
            method.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "utf-8");

            method.setRequestHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36");
            method.setRequestHeader("Accept-Charset", "utf-8,GB2312;q=0.7,*;q=0.7");
            method.setRequestHeader("Accept-Language", "zh-cn,zh;q=0.5");
            method.setRequestHeader("Connection", "Keep-Alive");
            method.setRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");


            for (Map.Entry<String, String> entry : headers.entrySet()) {
                method.setRequestHeader(entry.getKey(), entry.getValue());
            }

            int statusCode = client.executeMethod(method);

            if (statusCode == 200) {
                System.out.println("Notification sent successfully.");
                return method.getResponseBodyAsString();
            } else {
                System.out.println("Failed to send the notification!");
            }

            throw new IOException(method.getResponseBodyAsString());

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (client != null) {
                client.getHttpConnectionManager().closeIdleConnections(1);
            }
        }

        throw new IOException("网络异常");
    }

}
