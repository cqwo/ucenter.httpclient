package com.cqwo.ucenter.client.pages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author cqnews
 */
@Data
@AllArgsConstructor
public class PageInfo<T> implements Serializable {

    private static final long serialVersionUID = 6031542019881848367L;

    private Integer pageSize = 10;

    private Integer pageNumber = 1;

    private Integer totalCount = 0;

    private List<T> list;


}
