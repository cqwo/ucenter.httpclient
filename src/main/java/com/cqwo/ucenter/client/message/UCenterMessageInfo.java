/*
 *
 *  *
 *  *  * Copyright (C) 2018.
 *  *  * 用于JAVA项目开发
 *  *  * 重庆青沃科技有限公司 版权所有
 *  *  * Copyright (C)  2018.  CqingWo Systems Incorporated. All rights reserved.
 *  *
 *
 */

package com.cqwo.ucenter.client.message;

import com.alibaba.fastjson.JSON;
import com.cqwo.ucenter.client.exception.AnalyzeException;
import lombok.Data;

import java.io.Serializable;

@Data
public class UCenterMessageInfo implements Serializable {

    private static final long serialVersionUID = 3491696172535660022L;
    /**
     * 消息分类
     */
    private Integer type = 0;


    /**
     * 消息状态
     */
    private Integer state = -1;

    /**
     * 消息说明
     */
    private String message = "英卡欢迎您";


    /**
     * 消息正文
     */
    private String content = "";


    public UCenterMessageInfo() {

    }

    public UCenterMessageInfo(String message) {
        this.message = message;
    }


    public UCenterMessageInfo(Integer type, Integer state, String message) {
        this.type = type;
        this.state = state;
        this.message = message;
    }


    /**
     * 生成消息的来构造函数
     *
     * @param state   状态
     * @param message 消息
     */
    public static UCenterMessageInfo of(Integer state, String message) {

        UCenterMessageInfo UCenterMessageInfo = new UCenterMessageInfo();
        UCenterMessageInfo.setMessage(message);

        return UCenterMessageInfo;
    }

    /**
     * 解析Json
     *
     * @param s 原串
     */
    public static UCenterMessageInfo fromJson(String s) throws AnalyzeException {

        UCenterMessageInfo UCenterMessageInfo = new UCenterMessageInfo();

        try {

            UCenterMessageInfo = JSON.parseObject(s, UCenterMessageInfo.class);

        } catch (Exception ex) {
            throw new AnalyzeException("数据解析失败");
        }

        return UCenterMessageInfo;
    }

    public String toJson() {
        return JSON.toJSONString(this);
    }
}
