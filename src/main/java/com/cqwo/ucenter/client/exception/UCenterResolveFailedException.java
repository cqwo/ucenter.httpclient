package com.cqwo.ucenter.client.exception;

/**
 * 解析失败
 *
 * @author cqnews
 */
public class UCenterResolveFailedException extends UCenterException {

    private static final long serialVersionUID = 7895554127489170980L;

    public UCenterResolveFailedException() {
    }

    public UCenterResolveFailedException(String message) {
        super(message);
    }
}
