package com.cqwo.ucenter.client;

import com.cqwo.ucenter.client.domain.OauthInfo;
import com.cqwo.ucenter.client.domain.PartUserInfo;
import com.cqwo.ucenter.client.domain.RefreshTokenInfo;
import com.cqwo.ucenter.client.exception.UCenterException;
import com.cqwo.ucenter.client.message.UCenterMessageInfo;

import java.util.List;
import java.util.Map;

public interface UCenterHttpClient {

    /**
     * Post提交数据
     *
     * @param url    url地址
     * @param params 参数
     * @param clzz   类型
     * @param <T>    类型
     * @return 返回
     * @throws UCenterException 用户自定义异常
     */
    <T> T restPost(String url, Map<String, Object> params, Class<T> clzz) throws UCenterException;


    /**
     * Post提交数据
     *
     * @param url    url地址
     * @param params 参数
     * @param token  token
     * @param clzz   类型
     * @param <T>    类型
     * @return 返回
     * @throws UCenterException 用户自定义异常
     */
    <T> T restPost(String url, Map<String, Object> params, String token, Class<T> clzz) throws UCenterException;

    /**
     * Post提交数据
     *
     * @param url   url地址
     * @param token token
     * @param clzz  类型
     * @param <T>   类型
     * @return 返回
     */
    <T> T restGet(String url, String token, Class<T> clzz) throws UCenterException;

    /**
     * 获取ApiKey
     */
    String getAppId();

    /**
     * 获取ApiKey
     */
    String getApiKey();


    /**
     * 获取ApiSecret
     *
     * @return 返回getApiSecret
     */
    String getApiSecret();


    /**
     * 获取ApiURL
     *
     * @param address 地址
     */
    default String getApiUrl(String address) {
        return "https://ucenter.510link.com/api/" + address;
    }

    /**
     * 用户注册
     *
     * @param account  注册账号
     * @param password 密码
     * @param nickName 昵称
     * @param avatar   头像
     * @param gender   性别
     * @param regionId 区域
     * @throws UCenterException 用户自定义异常
     */
    void onRegister(String account,
                    String password,
                    String nickName,
                    String avatar,
                    Integer gender,
                    Integer regionId) throws UCenterException;

    /**
     * 用户注册
     *
     * @param account     注册账号
     * @param password    密码
     * @param nickName    昵称
     * @param avatar      头像
     * @param gender      性别
     * @param regionId    区域
     * @param expireHours 超时时间
     * @throws UCenterException 用户自定义异常
     */
    void onRegister(String account,
                    String password,
                    String nickName,
                    String avatar,
                    Integer gender,
                    Integer regionId, Integer expireHours) throws UCenterException;

    /**
     * 用户登录
     *
     * @param account  账号
     * @param password 密码
     * @throws UCenterException 用户自定义异常
     */
    void onLogin(String account, String password) throws UCenterException;

    /**
     * 用户登录
     *
     * @param account     账号
     * @param password    密码
     * @param expireHours 超时时间
     * @param isRefresh   是否刷新
     * @throws UCenterException 用户自定义异常
     */
    void onLogin(String account, String password, boolean isRefresh, Integer expireHours) throws UCenterException;


    /**
     * 用户token登录
     *
     * @param token token
     * @throws UCenterException 用户自定义异常
     */
    void onLogin(String token) throws UCenterException, UCenterException;


    /**
     * 开发接口登录
     *
     * @param openId   opendId
     * @param unionId  联合id
     * @param nickName 昵称
     * @param avatar   头像
     * @param gender   性别
     * @param regionId 区域
     * @throws UCenterException 用户自定义异常
     */
    UCenterMessageInfo onLogin(String server,
                               String openId,
                               String unionId,
                               String nickName,
                               String avatar,
                               Integer gender,
                               Integer regionId) throws UCenterException;

    /**
     * 开发接口登录
     *
     * @param openId      opendId
     * @param unionId     联合id
     * @param nickName    昵称
     * @param avatar      头像
     * @param gender      性别
     * @param regionId    区域
     * @param expireHours 超时时间
     * @throws UCenterException 用户自定义异常
     */
    UCenterMessageInfo onLogin(String server,
                               String openId,
                               String unionId,
                               String nickName,
                               String avatar,
                               Integer gender,
                               Integer regionId,
                               Integer expireHours) throws UCenterException;


    /**
     * 更新一条用户信息数据
     *
     * @param uid      更新用户uid
     * @param nickName 更新昵称
     * @param realName 更新真实姓名
     * @param regionId 更新区域Id
     * @param address  更新地址
     * @param bio      更新描述
     * @return 更新信息
     * @throws UCenterException 用户自定义异常
     */
    PartUserInfo updateUser(String uid,
                            String nickName,
                            String realName,
                            int regionId,
                            String address,
                            String bio) throws UCenterException;

    /**
     * 修改密码
     *
     * @param mobile      修改手机号码
     * @param newPassword 新密码
     * @return 返回状态
     * @throws UCenterException 异常
     */
    PartUserInfo updatePassword(String mobile,
                                String newPassword) throws UCenterException;


    /**
     * 修改用户密码
     *
     * @param uid         Uid
     * @param newPassword 新密码
     * @return 用户信息
     * @throws UCenterException UCenterException
     */
    PartUserInfo resetPassword(String uid,
                               String newPassword) throws UCenterException;

    /**
     * 刷新token
     *
     * @param uid          uid
     * @param refreshToken 刷新token
     * @return RefreshTokenInfo
     */
    RefreshTokenInfo refreshToken(String uid, String refreshToken) throws UCenterException;

    /**
     * 刷新token
     *
     * @param uid          uid
     * @param refreshToken 刷新token
     * @return RefreshTokenInfo
     */
    RefreshTokenInfo refreshToken(String uid, String refreshToken, Integer expireHours) throws UCenterException;

    /**
     * 通过用户uid获取用户信息
     *
     * @param uid uid
     * @return 用户信息
     * @throws UCenterException 用户自定义异常
     */
    PartUserInfo getPartUserByUid(String uid) throws UCenterException;

    /**
     * 通过手机号码获取用户
     *
     * @return 用户信息
     * @throws UCenterException 用户自定义异常
     */
    PartUserInfo getPartUserByMobile(String mobile) throws UCenterException;


    /**
     * 通过用户名获取用户
     *
     * @param userName 用户名
     * @return 用户信息
     * @throws UCenterException 用户自定义异常
     */
    PartUserInfo getPartUserByUserName(String userName) throws UCenterException;

    /**
     * 通过邮箱获取用户信息
     *
     * @param email 邮箱
     * @throws UCenterException 用户自定义异常
     */
    PartUserInfo getPartUserByEmail(String email) throws UCenterException;


    /**
     * 通过token获取用户信息
     *
     * @param openId openid
     * @throws UCenterException 用户自定义异常
     */
    PartUserInfo getPartUserByOpenId(String openId) throws UCenterException;


    /**
     * 通过unionId获取用户信息
     *
     * @param unionId unionId
     * @throws UCenterException 用户自定义异常
     */
    PartUserInfo getPartUserByUnionId(String unionId) throws UCenterException;


    /**
     * 获得用户数据列表
     *
     * @param uid      uid
     * @param nickName 昵称
     * @param mobile   手机号
     * @return 返回UserInfo
     * @throws UCenterException 用户自定义异常
     **/
    List<PartUserInfo> getPartUserList(String uid, String nickName, String mobile) throws UCenterException;


    /**
     * 获取用户列表
     *
     * @param pageSize   每页条数
     * @param pageNumber 当前页数
     * @param uid        uid
     * @param nickName   昵称
     * @param mobile     手机
     * @throws UCenterException 用户自定义异常
     */
    void getPartUserList(Integer pageSize, Integer pageNumber, String uid, String nickName, String mobile) throws UCenterException;

    /**
     * 更新手机
     *
     * @param uid    uid
     * @param mobile 手机
     * @throws UCenterException 用户自定义异常
     */
    void updateUserMobile(String uid, String mobile) throws UCenterException;

    /**
     * 获取用户头像
     *
     * @param uid uid
     * @throws UCenterException 用户自定义异常
     */
    String getUserAvatar(String uid) throws UCenterException;

    /**
     * 通过appid和uid查询第三方登录信息
     *
     * @param uid uid
     * @throws UCenterException 用户自定义异常
     */
    OauthInfo findOauthByUid(String uid) throws UCenterException;

    /**
     * 更新用户组
     *
     * @param uid     用户uid
     * @param userRid 用户分组
     * @throws UCenterException 用户自定义异常
     */
    void updateUserRankByUid(String uid, Integer userRid) throws UCenterException;

    /**
     * 更新用户金额
     *
     * @param uid   用户uid
     * @param money 金额
     * @throws UCenterException 用户自定义异常
     */
    void updateUserMoneyByUid(String uid, double money) throws UCenterException;
}
